package cz.dima.eshopbackend.repository;

import cz.dima.eshopbackend.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
