package cz.dima.eshopbackend.repository;

import cz.dima.eshopbackend.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
