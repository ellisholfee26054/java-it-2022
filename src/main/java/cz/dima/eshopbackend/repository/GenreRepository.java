package cz.dima.eshopbackend.repository;

import cz.dima.eshopbackend.domain.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, Long> {
}
