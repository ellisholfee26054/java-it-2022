package cz.dima.eshopbackend.repository;

import cz.dima.eshopbackend.domain.Cart;
import feign.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Long> {

    @Query(value = "SELECT c FROM Cart c WHERE c.modifiedAt < :modifiedTimestamp ")
    List<Cart> findAllByModifiedAtBefore(@Param("modifiedTimestamp") Instant modifiedTimestamp);
}

