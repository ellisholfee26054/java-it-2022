package cz.dima.eshopbackend.repository;

import cz.dima.eshopbackend.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
