package cz.dima.eshopbackend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class CartDto {
    private Long id;
    private Set<ProductSimpleDto> products;
}
