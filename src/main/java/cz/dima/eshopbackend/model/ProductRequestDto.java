package cz.dima.eshopbackend.model;

import cz.dima.eshopbackend.validation.StartsWithUppercase;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
public class ProductRequestDto {
    @NotBlank
    @Size(max = 256)
    @StartsWithUppercase
    private String name;
    @NotBlank
    @Size(max = 512)
    private String description;
    @NotBlank
    private String image;
    @NotNull
    @Min(1)
    private Long price;
    @Positive
    private Long stock;

    @NotNull
    private Long authorId;
    @NotNull
    private Long genreId;
}

