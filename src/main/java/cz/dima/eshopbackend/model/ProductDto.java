package cz.dima.eshopbackend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;


@Data
@NoArgsConstructor
public class ProductDto {
    private Long id;
    @NotBlank
    private String name;
    private String description;
    @NotBlank
    private String image;
    private Long price;
    private Long stock;

    private AuthorDto author;
    private GenreDto genre;
    private Boolean preview;
}

