package cz.dima.eshopbackend.model;

import lombok.Data;

@Data
public class ProductPreviewResponse {
    private String filename;
    private byte[] bytes;
}