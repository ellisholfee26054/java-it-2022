package cz.dima.eshopbackend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GenreDto {
    private Long id;
    private String name;
    private String description;
}
