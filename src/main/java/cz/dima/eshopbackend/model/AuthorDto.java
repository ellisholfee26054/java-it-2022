package cz.dima.eshopbackend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class AuthorDto {
    private Long id;
    private String name;
    private String bio;
    private LocalDate birthDate;
}

