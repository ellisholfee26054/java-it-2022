package cz.dima.eshopbackend.model;

import cz.dima.eshopbackend.domain.OrderStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Set;

@Data
@NoArgsConstructor
public class OrderDto {
    private Long id;
    private Set<ProductSimpleDto> products;
    private OrderStatus status;
    private Instant createdAt;
}
