package cz.dima.eshopbackend.exception;

import org.springframework.http.HttpStatus;

public class AuthorNotFoundException extends ItaException{
    public AuthorNotFoundException(Long id) {
        super("Author with id " + id + " not found!", "0001", HttpStatus.NOT_FOUND);
    }
}
