package cz.dima.eshopbackend.exception;

import org.springframework.http.HttpStatus;

public class GenreNotFoundException extends ItaException{

    public GenreNotFoundException(Long id) {
        super("Genre with id " + id + " not found!", "0001", HttpStatus.NOT_FOUND);
    }
}
