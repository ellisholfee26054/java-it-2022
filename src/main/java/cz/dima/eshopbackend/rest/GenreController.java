package cz.dima.eshopbackend.rest;

import cz.dima.eshopbackend.model.GenreDto;
import cz.dima.eshopbackend.service.impl.GenreServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/genres")
public class GenreController {
    private final GenreServiceImpl genreService;

    @GetMapping
    public Collection<GenreDto> findAllGenres() {
        return genreService.findAllGenres();
    }
}
