package cz.dima.eshopbackend.rest;


import cz.dima.eshopbackend.domain.OrderStatus;
import cz.dima.eshopbackend.model.OrderDto;
import cz.dima.eshopbackend.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("api/v1/orders")
@CrossOrigin("http://localhost:8088")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;


    @PostMapping("cart/{cartId}")
    public OrderDto createOrder(@PathVariable Long cartId) {
        return orderService.createOrder(cartId);
    }


    @GetMapping
    public Collection<OrderDto> findAllOrders() {
        return orderService.findAllOrders();
    }

    @PutMapping("/{id}/status/{status}")
    public OrderDto updateStatus(@PathVariable("id") Long orderId, @PathVariable("status") OrderStatus orderStatus) {
        return orderService.updateStatus(orderId, orderStatus);
    }
}
