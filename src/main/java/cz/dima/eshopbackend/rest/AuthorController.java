package cz.dima.eshopbackend.rest;

import cz.dima.eshopbackend.model.AuthorDto;
import cz.dima.eshopbackend.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/authors")
public class AuthorController {
    private final AuthorService authorService;

    @GetMapping
    public Collection<AuthorDto> findAllAuthors() {
        return authorService.findAllAuthors();
    }
}

