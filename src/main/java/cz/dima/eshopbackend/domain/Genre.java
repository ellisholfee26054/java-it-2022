package cz.dima.eshopbackend.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Genre extends AbstractEntity {
    private String name;
    private String description;
}

