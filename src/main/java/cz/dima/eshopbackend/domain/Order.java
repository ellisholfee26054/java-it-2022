package cz.dima.eshopbackend.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "eshop_order")
public class Order extends AbstractEntity {
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @ManyToMany
    @JoinTable(
            name = "eshop_order_products",
            joinColumns = @JoinColumn(name = "fk_eshop_order"),
            inverseJoinColumns = @JoinColumn(name = "fk_product"))
    private Set<Product> products;
}

