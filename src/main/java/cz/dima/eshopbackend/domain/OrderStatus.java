package cz.dima.eshopbackend.domain;

public enum OrderStatus {
    NEW, COMPLETED, CANCELED
}
