package cz.dima.eshopbackend.service;

import cz.dima.eshopbackend.model.CartDto;
import cz.dima.eshopbackend.model.ProductRequestDto;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;

/**
 * A service for manipulating carts
 */
public interface CartService {
    /**
     * Retrieves specific cart
     * @param id of cart
     * @return {@link CartDto}
     */
    CartDto findCart(Long id);

    /**
     * Creates new cart for given product
     * @return {@link CartDto}
     */
    CartDto createCart(Long productId);

    /**
     * Adds product to cart
     * @param id of cart
     * @return {@link  CartDto}
     */
    CartDto addToCart(Long id,Long productId);


    /**
     * Removes all unused carts before target timestamp
     */
    @Transactional
    void deleteUnusedCarts(Instant beforeInstant);
}
