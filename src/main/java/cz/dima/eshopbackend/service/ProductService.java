package cz.dima.eshopbackend.service;

import cz.dima.eshopbackend.model.ProductPreviewResponse;
import cz.dima.eshopbackend.model.ProductRequestDto;
import cz.dima.eshopbackend.model.ProductDto;
import cz.dima.eshopbackend.model.ProductSimpleDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;

/**
 * A service for manipulating products
 */
public interface ProductService {
    /**
     * Retrieves specific product from database
     * @param id of product
     * @return {@link ProductDto}
     */
    ProductDto findProduct(Long id);

    /**
     * Retrieves all products from database
     * @see ProductSimpleDto
     * @return {@link java.util.List} of {@link ProductSimpleDto} if no product is present, returns empty list
     */
    Collection<ProductSimpleDto> findAllProducts();

    /**
     * Creates new product. Uses {@link ProductRequestDto} as param for cleaner json requests.
     * @param productDto ({@link ProductRequestDto})
     * @return {@link ProductDto}
     */
    ProductDto createProduct(ProductRequestDto productDto);

    /**
     * Updates product
     * @param productDto ({@link ProductRequestDto})
     * @param id of product
     * @return {@link ProductDto}
     */
    ProductDto updateProduct(ProductRequestDto productDto, Long id);


    /**
     * Deletes product
     * @param id of product
     */
    void deleteProduct(Long id);

    /**
     * Adds preview for product
     * @param id of product
     */
    void addPreview(Long id, MultipartFile file);
    /**
     * Retrieves preview of the product
     * @param id of product
     * @return {@link ProductPreviewResponse}
     */
    ProductPreviewResponse getPreview(Long id);

    /**
     * Updates all products stock from warehouse client
     */
    void updateStockFromWarehouse();
}

