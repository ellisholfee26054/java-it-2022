package cz.dima.eshopbackend.service;

import cz.dima.eshopbackend.model.GenreDto;

import java.util.Collection;
import java.util.List;

/**
 * A service for manipulating genres
 */
public interface GenreService {
    /**
     * Retrieves all genres from database
     * @return {@link java.util.List} of {@link GenreDto}
     */
    Collection<GenreDto> findAllGenres();
}
