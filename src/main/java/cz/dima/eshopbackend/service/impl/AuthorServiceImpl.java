package cz.dima.eshopbackend.service.impl;

import cz.dima.eshopbackend.domain.Author;
import cz.dima.eshopbackend.mapper.AuthorMapper;
import cz.dima.eshopbackend.model.AuthorDto;
import cz.dima.eshopbackend.repository.AuthorRepository;
import cz.dima.eshopbackend.service.AuthorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    @Override
    @Transactional(readOnly = true)
    public Collection<AuthorDto> findAllAuthors() {
        log.info("Fetching all authors");
        return authorRepository.findAll().stream()
                .map(authorMapper::toDto)
                .collect(Collectors.toList());
    }
}

