package cz.dima.eshopbackend.service.impl;

import cz.dima.eshopbackend.mapper.GenreMapper;
import cz.dima.eshopbackend.model.GenreDto;
import cz.dima.eshopbackend.repository.GenreRepository;
import cz.dima.eshopbackend.service.GenreService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class GenreServiceImpl implements GenreService {
    private final GenreRepository genreRepository;
    private final GenreMapper genreMapper;

    @Override
    @Transactional(readOnly = true)
    public Collection<GenreDto> findAllGenres() {
        log.info("Fetching all genres");
        return genreRepository.findAll().stream()
                .map(genreMapper::toDto)
                .collect(Collectors.toList());
    }
}


