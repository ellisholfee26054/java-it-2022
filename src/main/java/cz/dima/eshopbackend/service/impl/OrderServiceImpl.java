package cz.dima.eshopbackend.service.impl;

import cz.dima.eshopbackend.domain.Cart;
import cz.dima.eshopbackend.domain.Order;
import cz.dima.eshopbackend.domain.OrderStatus;
import cz.dima.eshopbackend.exception.CartNotFoundException;
import cz.dima.eshopbackend.exception.OrderNotFoundException;
import cz.dima.eshopbackend.mapper.OrderMapper;
import cz.dima.eshopbackend.model.OrderDto;
import cz.dima.eshopbackend.repository.CartRepository;
import cz.dima.eshopbackend.repository.OrderRepository;
import cz.dima.eshopbackend.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final CartRepository cartRepository;
    private final OrderMapper orderMapper;

    @Override
    @Transactional
    public OrderDto createOrder(Long cartId) {
        log.info("Creating order in cart {}", cartId);
        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new CartNotFoundException(cartId));
        Order order = new Order()
                .setStatus(OrderStatus.NEW)
                .setProducts(new HashSet<>(cart.getProducts()));
        orderRepository.save(order);
        cartRepository.deleteById(cartId);
        return orderMapper.toDto(order);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderDto> findAllOrders() {
        return orderRepository.findAll().stream()
                .map(orderMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public OrderDto updateStatus(Long orderId, OrderStatus status) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new OrderNotFoundException(orderId));
        order.setStatus(status);
        System.out.println(order.getStatus());
        return orderMapper.toDto(order);
    }
}

