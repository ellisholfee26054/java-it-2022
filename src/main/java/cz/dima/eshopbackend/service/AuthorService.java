package cz.dima.eshopbackend.service;

import cz.dima.eshopbackend.model.AuthorDto;

import java.util.Collection;

/**
 * A service for manipulating Authors
 */
public interface AuthorService {
    /**
     * Retrieves all authors from database
     * @return {@link java.util.List} of {@link AuthorDto}
     */
    Collection<AuthorDto> findAllAuthors();
}
