package cz.dima.eshopbackend.service;

import cz.dima.eshopbackend.model.OrderDto;
import cz.dima.eshopbackend.domain.OrderStatus;

import java.util.List;


/**
 * A service for manipulating orders
 */
public interface OrderService {
    /**
     * Creates new order upon given cart.
     * All new orders have {@link OrderStatus}.NEW.
     * @return {@link List} of {@link OrderDto}
     */
    OrderDto createOrder(Long cartId);

    /**
     * Retrieves all orders from database
     * @return {@link List} of {@link OrderDto}
     */
    List<OrderDto> findAllOrders();

    /**
     * Updates status of specific order.
     * @param status to update Order status
     * @return {@link OrderDto}
     */
    OrderDto updateStatus(Long orderId, OrderStatus status);
}

