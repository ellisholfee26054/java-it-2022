package cz.dima.eshopbackend.mapper;

import cz.dima.eshopbackend.domain.Genre;
import cz.dima.eshopbackend.model.GenreDto;
import org.mapstruct.Mapper;

@Mapper
public interface GenreMapper {

    GenreDto toDto(Genre genre);

    Genre toDomain(GenreDto genreDto);

}
