package cz.dima.eshopbackend.mapper;

import cz.dima.eshopbackend.domain.Order;
import cz.dima.eshopbackend.model.OrderDto;
import org.mapstruct.Mapper;

@Mapper
public interface OrderMapper {

    OrderDto toDto(Order order);

    Order toDomain(OrderDto orderDto);
}
