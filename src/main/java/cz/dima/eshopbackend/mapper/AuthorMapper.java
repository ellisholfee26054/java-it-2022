package cz.dima.eshopbackend.mapper;

import cz.dima.eshopbackend.domain.Author;
import cz.dima.eshopbackend.model.AuthorDto;
import org.mapstruct.Mapper;

@Mapper
public interface AuthorMapper {
    AuthorDto toDto(Author author);
    Author toDomain(AuthorDto authorDto);
}
