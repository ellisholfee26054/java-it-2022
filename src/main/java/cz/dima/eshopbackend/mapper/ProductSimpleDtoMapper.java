package cz.dima.eshopbackend.mapper;

import cz.dima.eshopbackend.domain.Product;
import cz.dima.eshopbackend.model.ProductSimpleDto;
import org.mapstruct.Mapper;

@Mapper
public interface ProductSimpleDtoMapper {

    ProductSimpleDto toDto(Product product);

    Product toDomain(ProductSimpleDto productSimple);
}
