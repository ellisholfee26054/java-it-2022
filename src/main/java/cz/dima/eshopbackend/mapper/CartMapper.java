package cz.dima.eshopbackend.mapper;

import cz.dima.eshopbackend.domain.Cart;
import cz.dima.eshopbackend.model.CartDto;
import org.mapstruct.Mapper;

@Mapper
public interface CartMapper {

    CartDto toDto(Cart cart);

    Cart toDomain(CartDto cartDto);
}
