package cz.dima.eshopbackend.mapper;

import cz.dima.eshopbackend.domain.Product;
import cz.dima.eshopbackend.model.ProductDto;
import cz.dima.eshopbackend.model.ProductRequestDto;
import cz.dima.eshopbackend.model.ProductSimpleDto;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper
public interface ProductMapper {
    @BeforeMapping
    default void beforeMapping(@MappingTarget ProductDto target, Product source) {
        target.setPreview(source.getPreview_file_name() != null);
    }

    @Mapping(target = "preview", ignore = true)
    ProductDto toDto(Product domain);
    ProductSimpleDto toSimpleDto(Product domain);
    Product toDomain(ProductRequestDto dto);
    void mergeProduct(@MappingTarget Product target, ProductRequestDto source);
}

