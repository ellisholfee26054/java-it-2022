package cz.dima.eshopbackend.mapper.job;

import cz.dima.eshopbackend.service.impl.CartServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;


@Component
@RequiredArgsConstructor
@Slf4j
public class CartRemovingJob {
    private final CartServiceImpl cartService;

    @Scheduled(cron = "${app.jobs.cleanUnusedCartsWithCron}")
    private void cleanUnusedCarts() {
        cartService.deleteUnusedCarts(java.time.Instant.now().minus(10, ChronoUnit.MINUTES));
    }
}

