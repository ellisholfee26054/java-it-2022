package cz.dima.eshopbackend.mapper.job;

import cz.dima.eshopbackend.service.impl.ProductServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ProductUpdatingJob {

    private final ProductServiceImpl productService;

    @Scheduled(cron = "${app.jobs.updateStockWithCron}")
    private void updateProductStockFromWarehouse() {
        log.info("Updating stock from warehouse");
        productService.updateStockFromWarehouse();
    }
}
