package cz.dima.eshopbackend.service.impl;

import cz.dima.eshopbackend.domain.Cart;
import cz.dima.eshopbackend.domain.Order;
import cz.dima.eshopbackend.domain.Product;
import cz.dima.eshopbackend.mapper.OrderMapper;
import cz.dima.eshopbackend.model.OrderDto;
import cz.dima.eshopbackend.repository.CartRepository;
import cz.dima.eshopbackend.repository.OrderRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.Set;

import static cz.dima.eshopbackend.mother.CartMother.prepareCart1;
import static cz.dima.eshopbackend.mother.OrderMother.prepareOrder;
import static cz.dima.eshopbackend.mother.OrderMother.prepareOrderDto;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest implements WithAssertions {

    @Captor
    ArgumentCaptor<Order> orderCaptor;
    @InjectMocks
    private OrderServiceImpl orderService;
    @Mock
    private OrderRepository orderRepository;
    @Mock
    private CartRepository cartRepository;
    @Mock
    private OrderMapper orderMapper;

    @Test
    void createOrder() {
        Cart cart = prepareCart1();
        Long cartId = 1L;
        OrderDto expectedResult = prepareOrderDto();
        Set<Product> expectedProductsInOrder = prepareOrder().getProducts();

        when(cartRepository.findById(cartId)).thenReturn(Optional.of(cart));
        when(orderMapper.toDto(any(Order.class))).thenReturn(expectedResult);

        OrderDto result = orderService.createOrder(cartId);

        verify(orderRepository).save(orderCaptor.capture()); // Capture saved order

        assertThat(result).usingRecursiveComparison().isEqualTo(expectedResult);
        assertThat(orderCaptor.getValue().getProducts()).usingRecursiveComparison().isEqualTo(expectedProductsInOrder);

        verify(cartRepository).findById(cartId);
        verify(cartRepository).deleteById(cartId);
    }
}