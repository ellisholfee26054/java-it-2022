package cz.dima.eshopbackend.service.impl;

import cz.dima.eshopbackend.domain.Author;
import cz.dima.eshopbackend.mapper.AuthorMapper;
import cz.dima.eshopbackend.model.AuthorDto;
import cz.dima.eshopbackend.repository.AuthorRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.List;

import static cz.dima.eshopbackend.mother.AuthorMother.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthorServiceImplTest implements WithAssertions {
    @InjectMocks
    private AuthorServiceImpl authorService;
    @Mock
    private AuthorRepository authorRepository;
    @Mock
    private AuthorMapper authorMapper;

    @Test
    void findAllAuthors() {
        Author author = prepareAuthor();
        Author author1 = prepareAuthor1();
        AuthorDto authorDto = prepareAuthorDto();
        AuthorDto authorDto1 = prepareAuthorDto1();

        Collection<AuthorDto> expectedResult = List.of(authorDto, authorDto1);

        when(authorRepository.findAll()).thenReturn(List.of(author, author1));
        when(authorMapper.toDto(author)).thenReturn(authorDto);
        when(authorMapper.toDto(author1)).thenReturn(authorDto1);

        Collection<AuthorDto> result = authorService.findAllAuthors();

        assertThat(result).usingRecursiveComparison().isEqualTo(expectedResult);

        verify(authorRepository).findAll();
        verify(authorMapper).toDto(author);
        verify(authorMapper).toDto(author1);
    }
}