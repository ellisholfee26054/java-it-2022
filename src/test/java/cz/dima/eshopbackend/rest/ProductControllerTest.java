package cz.dima.eshopbackend.rest;

import cz.dima.eshopbackend.exception.ProductNotFoundException;
import cz.dima.eshopbackend.model.ProductDto;
import cz.dima.eshopbackend.model.ProductRequestDto;
import cz.dima.eshopbackend.model.ProductSimpleDto;
import cz.dima.eshopbackend.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static cz.dima.eshopbackend.mother.ProductMother.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
class ProductControllerTest extends AbstractMvcTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ProductServiceImpl productService;
    private final String adminAuthToken = "Basic YWRtaW46cGFzc3dvcmQ="; // Base 64

    @Test
    void findProduct() throws Exception {
        ProductDto productDto = prepareProductDto();
        when(productService.findProduct(1L)).thenReturn(productDto);
        mockMvc.perform(get("/api/v1/products/1"))
                .andExpect(status().isOk());
    }

    @Test
    void findProduct_notFound() throws Exception {
        when(productService.findProduct(1L)).thenThrow(new ProductNotFoundException(1L));
            mockMvc.perform(get("/api/v1/products/1"))
                .andExpect(status().isNotFound());


    }

    @Test
    void findAllProducts() throws Exception {
        List<ProductSimpleDto> productSimpleDtos = List.of(prepareProductSimpleDto(), prepareProductSimpleDto1());
        when(productService.findAllProducts()).thenReturn(productSimpleDtos);
        mockMvc.perform(get("/api/v1/products"))
                .andExpect(status().isOk());
    }

    @Test
    void createProduct() throws Exception {
        ProductRequestDto productRequestDto = prepareProductRequestDto();
        ProductDto productDto = prepareProductDto();
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, adminAuthToken);
        when(productService.createProduct(productRequestDto)).thenReturn(productDto);
        mockMvc.perform(post("/api/v1/products")
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getJsonContent("/requests/products/createProduct.json")))
                .andExpect(status().isOk());
    }

    @Test
    void updateProduct() throws Exception {
        ProductRequestDto productRequestDto = prepareProductRequestDto();
        ProductDto productDto = prepareProductDto();
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, adminAuthToken);
        when(productService.updateProduct(productRequestDto, 1L)).thenReturn(productDto);
        mockMvc.perform(put("/api/v1/products/1")
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getJsonContent("/requests/updateProduct.json")))
                .andExpect(status().isOk())
                .andExpect(content().json(getJsonContent("/responses/products/updateProduct.json")));
    }


    @Test
    void deleteProduct() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, adminAuthToken);
        mockMvc.perform(delete("/api/v1/products/1")
                        .headers(headers))
                .andExpect(status().isNoContent());
    }
}