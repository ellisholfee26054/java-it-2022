package cz.dima.eshopbackend.rest;


import cz.dima.eshopbackend.configuration.SecurityConfiguration;
import cz.dima.eshopbackend.configuration.SecurityConfigurationProperties;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

@Import({SecurityConfiguration.class, SecurityConfigurationProperties.class})
public abstract class AbstractMvcTest {
    public String getJsonContent(String resources) throws IOException, URISyntaxException {
        return Files.readString(Paths.get(Objects.requireNonNull(getClass().getResource(resources)).toURI()));
    }
}
