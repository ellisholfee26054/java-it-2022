package cz.dima.eshopbackend.mother;

import cz.dima.eshopbackend.domain.Order;
import cz.dima.eshopbackend.domain.OrderStatus;
import cz.dima.eshopbackend.model.OrderDto;
import org.apache.commons.lang3.RandomUtils;

import java.util.Set;

import static cz.dima.eshopbackend.mother.ProductMother.*;


public class OrderMother {
    public static Order prepareOrder() {
        Order order = new Order()
                .setProducts(Set.of(prepareProduct(), prepareProduct1()))
                .setStatus(OrderStatus.NEW);
        order.setId(1L);
        return order;
    }
    public static OrderDto prepareOrderDto() {
        return new OrderDto()
                .setId(1L)
                .setProducts(Set.of(prepareProductSimpleDto(), prepareProductSimpleDto1()))
                .setStatus(OrderStatus.NEW)
                .setCreatedAt(null);
    }
}

