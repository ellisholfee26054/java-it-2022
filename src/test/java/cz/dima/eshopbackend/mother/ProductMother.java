package cz.dima.eshopbackend.mother;

import cz.dima.eshopbackend.domain.Product;
import cz.dima.eshopbackend.model.ProductDto;
import cz.dima.eshopbackend.model.ProductRequestDto;
import cz.dima.eshopbackend.model.ProductSimpleDto;
import lombok.NoArgsConstructor;

import static cz.dima.eshopbackend.mother.AuthorMother.*;
import static cz.dima.eshopbackend.mother.GenreMother.*;

public class ProductMother {
    public static Product prepareProduct() {
        Product product = new Product()
                .setName("ZaДЌГ­nГЎme programovat v jazyku Java")
                .setDescription("Tato publikace uvГЎdГ­ ДЌtenГЎЕ™e do svД›ta programovГЎnГ­...")
                .setImage("https://img.grada.cz/_t_/media/sprinx.bookimages/130627_350_0_fit.jpg")
                .setPrice(450L)
                .setStock(21L)
                .setAuthor(AuthorMother.prepareAuthor())
                .setGenre(prepareGenre());
        product.setId(1L);
        return product;
    }
    public static Product prepareProduct1() {
        Product product = new Product()
                .setName("Pat a Mat nГЎs bavГ­ ")
                .setDescription("Pat a Mat vГЎm ve tЕ™inГЎcti novГЅch epizodГЎch plnГЅch skvД›lГЅch gagЕЇ opД›t dokГЎЕѕГ­, jacГ­ jsou to ЕЎikulovГ©.")
                .setImage("https://mrtns.eu/tovar/_l/463/l463501.jpg?v=16596936211")
                .setPrice(249L)
                .setStock(1432L)
                .setAuthor(prepareAuthor1())
                .setGenre(prepareGenre1());
        product.setId(2L);
        return product;
    }

    public static ProductDto prepareProductDto() {
        return new ProductDto()
                .setId(1L)
                .setName("ZaДЌГ­nГЎme programovat v jazyku Java")
                .setDescription("Tato publikace uvГЎdГ­ ДЌtenГЎЕ™e do svД›ta programovГЎnГ­...")
                .setImage("https://img.grada.cz/_t_/media/sprinx.bookimages/130627_350_0_fit.jpg")
                .setPrice(450L)
                .setStock(21L)
                .setAuthor(prepareAuthorDto())
                .setGenre(prepareGenreDto())
                .setPreview(false);
    }
    public static ProductDto prepareProductDto1() {
        return new ProductDto()
                .setId(2L)
                .setName("Pat a Mat nГЎs bavГ­ ")
                .setDescription("Pat a Mat vГЎm ve tЕ™inГЎcti novГЅch epizodГЎch plnГЅch skvД›lГЅch gagЕЇ opД›t dokГЎЕѕГ­, jacГ­ jsou to ЕЎikulovГ©.")
                .setImage("https://mrtns.eu/tovar/_l/463/l463501.jpg?v=16596936211")
                .setPrice(249L)
                .setStock(1432L)
                .setAuthor(prepareAuthorDto1())
                .setGenre(prepareGenreDto1())
                .setPreview(false);
    }
    public static ProductRequestDto prepareProductRequestDto() {
        return new ProductRequestDto()
                .setName("ZaДЌГ­nГЎme programovat v jazyku Java")
                .setDescription("Tato publikace uvГЎdГ­ ДЌtenГЎЕ™e do svД›ta programovГЎnГ­...")
                .setImage("https://img.grada.cz/_t_/media/sprinx.bookimages/130627_350_0_fit.jpg")
                .setPrice(450L)
                .setStock(21L)
                .setAuthorId(1L)
                .setGenreId(1L);
    }
    public static ProductRequestDto prepareProductRequestDto1() {
        return new ProductRequestDto()
                .setName("Book that you love")
                .setDescription("Pat a Mat vГЎm ve tЕ™inГЎcti novГЅch epizodГЎch plnГЅch skvД›lГЅch gagЕЇ opД›t dokГЎЕѕГ­, jacГ­ jsou to ЕЎikulovГ©.")
                .setImage("https://mrtns.eu/tovar/_l/463/l463501.jpg?v=16596936211")
                .setPrice(249L)
                .setStock(1432L)
                .setAuthorId(2L)
                .setGenreId(2L);
    }
    public static ProductSimpleDto prepareProductSimpleDto() {
        return new ProductSimpleDto()
                .setId(1L)
                .setName("Java lan")
                .setImage("https://img.grada.cz/_t_/media/sprinx.bookimages/130627_350_0_fit.jpg")
                .setPrice(450L);
    }
    public static ProductSimpleDto prepareProductSimpleDto1() {
        return new ProductSimpleDto()
                .setId(2L)
                .setName("Swift lan")
                .setImage("https://mrtns.eu/tovar/_l/463/l463501.jpg?v=16596936211")
                .setPrice(249L);
    }
}

